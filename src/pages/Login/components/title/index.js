import React from 'react';

const Title = ({text})=>{

    return(
        <div>
            <h2><label>{text}</label></h2>
        </div>
    )
};
export default Title; 
