import React from "react";

const Input = ({ atribute, hadleChange, param }) => {
  return (
    <div>
      <input
      id={atribute.id}
      name={atribute.name}
      placeholder={atribute.placeholder}
      type={atribute.type}
      onChange={e=> hadleChange(e.target.name, e.target.value)}
      className="form-control" />
      {param ? 
      <div class="alert alert-danger" role="alert">
  error en contraseña!
</div>: null}
    </div>
  );
};
export default Input;