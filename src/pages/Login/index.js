import React, { useState } from "react";
import Title from "./components/title";
import Label from "./components/label";
import Input from "./components/input";
const Login = () => {
  const [user, setUser] = useState("");
  const [pass, setPass] = useState("");
  const [passError, setPassError] = useState(false);
  const [isLoggin, setIsLoggin] = useState(false);
  const [hasError,setHasError] = useState(false);


  const change = (name, value) => {
    if (name === "usuario") {
      setUser(value);
    } else {
     
      if (value.length < 6) {
        setPassError(true);
      }else{
         setPass(value);
        setPassError(false);
      }
    }
  };
  const handleSubmit = () => {
    let acconut = { user, pass };
    if (acconut) {
      ifMatch(acconut);
    }
  };
  const ifMatch = (params)=>{
    if(params.user.length >0 && params.pass.length >0){
      if(params.user === "carol" && params.pass =="123456"){
        const { user, pass } = params;
        let ac = { user, pass};
        let account = JSON.stringify(ac);
        localStorage.setItem("account", account);
        setIsLoggin(true);
      }else{
        setIsLoggin(false);
        setHasError(true);
      }
    }else{
        setIsLoggin(false);
        setHasError(true);

    }
  };
  return (
    <div className="row">
      {isLoggin ? (
        <div className="col-md-10 m-4">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
              Navbar
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#">
                    Home <span class="sr-only">(current)</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    Features
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    Pricing
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link disabled"
                    href="#"
                    tabindex="-1"
                    aria-disabled="true"
                  >
                    Disabled
                  </a>
                </li>
              </ul>
            </div>
          </nav>
          <a href="#" class="text-decoration-none">
            <h2>Estas logeado!</h2>
          </a>
          
        </div>
      ) : (
        <div className="col-md-4 m-4">
          <div className="card">
            <Title className="card-header" text="hola title" />
            {hasError ? (
              <div class="alert alert-warning" role="alert">
                error en contraseña o usuario!
              </div>
            ) : null}
            <div className="card-body">
              <div className="form-group">
                <Label className="form-control" text="usuario" />
                <Input
                  atribute={{
                    id: 1,
                    type: "text",
                    placeholder: "usuario",
                    name: "usuario",
                  }}
                  hadleChange={change}
                />
                <Label className="form-control" text="contraseña" />
                <Input
                  atribute={{
                    id: 2,
                    type: "password",
                    placeholder: "contraseña",
                    name: "pass",
                  }}
                  hadleChange={change}
                  param={passError}
                />
              </div>
              <button className="btn btn-primary" onClick={handleSubmit}>
                Submit
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default Login;
